<?php

include '../conf.php';

function new_time($a) { // преобразовываем время в нормальный вид
  $ndate = date('d.m.Y', $a);
  $ndate_time = date('H:i', $a);
  $ndate_exp = explode('.', $ndate);
  $nmonth = array('янв', 'фев', 'мар', 'апр', 'мая', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');
  $nmonth_name = $nmonth[intval($ndate_exp[1]) - 1];
  if ($ndate == date('d.m.Y')) return 'сегодня в '.$ndate_time;
  elseif($ndate == date('d.m.Y', strtotime('-1 day'))) return 'вчера в '.$ndate_time;
  else return $ndate_exp[0].' '.$nmonth_name.' '.$ndate_exp[2].' в '.$ndate_time;
}


function get_data(){
  global $mysqli;
  date_default_timezone_set('Asia/Yekaterinburg');
  $res = $mysqli->query('SELECT * FROM `users`');
  $ids = array();
  $times = array();
  while ($row = $res->fetch_assoc()){
    $ids[] = (int)$row['id'];
    $times[] = $row['time'];
  }
  $hows = array();
  $res = $mysqli->query('SELECT `id`, COUNT(`id`) as `hows` FROM `users` GROUP BY `id`');
  while ($row = $res->fetch_assoc()){
    $hows[$row['id']] = $row['hows'];
  }
  $info = json_decode(file_get_contents('https://api.vk.com/method/users.get?v=5.45&lang=ru&fields=photo_50,bdate,sex&user_ids='.implode(',', $ids)))->response;
  $keyInfo  = array();
  foreach ($info as $item)
    $keyInfo[(int)($item->id)] = $item;
  $datas = array();
  for ($i = 0; $i < count($ids); $i++) {
    $id = $ids[$i];
    $time = $times[$i];
    $curr = $keyInfo[$id];
    $datas[] = array(
      'id' => $id,
      'photo' => $curr->photo_50,
      'name' => $curr->first_name . ' ' . $curr->last_name,
      'sex' => ($curr->sex == 1 ? 'жен' : ($curr->sex == 2 ? 'муж' : '-')),
      'bdate' => (isset($curr->bdate) && preg_match('/^\d+\.\d+\.\d+$/', $curr->bdate))
                 ? floor((time() - strtotime($curr->bdate)) / 31556926)
                 : '-',
      'lastComes' => new_time(strtotime($time)),
      'howComes' => $hows[$id],
      'rawTime' => strtotime($time)
    );
  }
  return $datas;
}


?>
