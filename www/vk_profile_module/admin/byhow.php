<?php
  include '_helpers.php';
  $datas = get_data();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Админка профилей ВК посетителей сайта "Антей"</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.9.7/chartist.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.9.7/chartist.min.js"></script>
    <link rel='stylesheet' href='style.css'>
  </head>
  <body>
    <div class='container-fluid'>
      <div class='row'>

        <div class='col-xs-3 sidebar'>
          <div class='nav'>
            <li><a href='index.php'>Список посетителей</a></li>
            <li><a href='thebest.php'>Наиболее часто посещающие</a></li>
          </div>
          <br>
          <h4 class='p-l-15'>Статистика</h4>
          <div class='nav'>
            <li class='active'><a href='byhow.php'>По количеству посещений</a></li>
            <li><a href='bygender.php'>По полу/возрасту</a></li>
          </div>
        </div>

        <div class='col-xs-9 col-xs-offset-3 content'>
          <h4>Количество посещений по дням за последние 2 недели</h4>
          <div id='age_graph'></div>
          <h4>Количество посещений по месяцам</h4>
          <div id='age_bar'></div>

          <script>
            Date.prototype.getMonthName = function(){
              var month = ['янв','фев','мар','апр','май','июн', 'июл','авг','сен','окт','ноя','дек'];
              return month[this.getMonth()];
            }
            <?php echo 'var data = '.json_encode($datas).';'; ?>

            var howDays = 14;
            var howMonths = 6;

            var now = Date.now();
            var nowObj = new Date();
            var d = nowObj.getDate(), m = nowObj.getMonth(), y = nowObj.getYear();

            var labels = [];
            var series = [];
            for (var i=howDays; i>=0; --i) {
              var date = new Date(now - i * 24 * 3600 * 1000);
              var month = date.getMonthName();
              var day = date.getDate();
              labels.push(day + ' ' + month);
              series.push(0);
            }

            var barLabels = [];
            var barSeries = [];
            for (var i=howMonths-1; i>=0; --i){
              var tDate = new Date(y, m-i, d);
              barSeries.push(0);
              barLabels.push(tDate.getMonthName());
            }

            function monthDiff(d1, d2) {
              var months;
              months = (d2.getFullYear() - d1.getFullYear()) * 12;
              months -= d1.getMonth() + 1;
              months += d2.getMonth();
              return months <= 0 ? 0 : months;
            }

            data.forEach(function(item){
              var tDate = new Date(item.rawTime * 1000);
              var tDateDiff = now - tDate;
              var days = Math.floor(tDateDiff / 1000 / 3600 / 24);
              var months = monthDiff(tDate, nowObj);
              if (days >= 0 && days <= howDays)
                ++series[days];
              if (months <= howMonths)
                ++barSeries[months];
            });


            new Chartist.Line('#age_graph', {
              labels: labels,
              series: [series]
            });

            new Chartist.Bar('#age_bar', {
              labels: barLabels,
              series: [barSeries]
            }, {

            });
          </script>
        </div>

      </div>
    </div>
  </body>
</html>
