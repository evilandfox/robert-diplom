<?php

  header('Content-Type: application/javascript');
  include 'conf.php';

  $currIds = json_decode(file_get_contents("https://api.vk.com/method/likes.getList?v=5.45&type=sitepage&owner_id=$appId&page_url=$page_url"))->response->items;

  $prevIds = array_map(function($elem){
    return (int)$elem;
  }, file('ids_prev_list.txt'));

  $idsDiff = array_merge(array_diff($prevIds, $currIds), array_diff($currIds, $prevIds));

  if (count($idsDiff) == 1)
    $mysqli->query("INSERT INTO `users`(`id`) VALUES ({$idsDiff[0]})");

  file_put_contents('ids_prev_list.txt', join("\n", $currIds));

?>
