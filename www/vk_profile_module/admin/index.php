<?php
  include '_helpers.php';

  $datas = get_data();
  usort($datas, function($a, $b){
    return $b['rawTime'] - $a['rawTime'];
  });

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Админка профилей ВК посетителей сайта "Антей"</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <link rel='stylesheet' href='style.css'>
  </head>
  <body>
    <div class='container-fluid'>
      <div class='row'>

        <div class='col-xs-3 sidebar'>
          <div class='nav'>
            <li class='active'><a href='index.php'>Список посетителей</a></li>
            <li><a href='thebest.php'>Наиболее часто посещающие</a></li>
          </div>
          <br>
          <h4 class='p-l-15'>Статистика</h4>
          <div class='nav'>
            <li><a href='byhow.php'>По количеству посещений</a></li>
            <li><a href='bygender.php'>По полу/возрасту</a></li>
          </div>
        </div>

        <div class='col-xs-9 col-xs-offset-3 content'>
          <table class='table table-hover'>
            <thead>
              <tr>
                <th>Фото</th>
                <th>Фамилия и имя</th>
                <th>Пол</th>
                <th>Возраст</th>
                <th>Время захода</th>
                <th>Всего заходов</th>
                <th>Профиль</th>
                <th>Диалог</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($datas as $data) { ?>
              <tr>
                <td><img src='<?php echo $data['photo']; ?>' class='img-rounded'></td>
                <td><?php echo $data['name']; ?></td>
                <td><?php echo $data['sex']; ?></td>
                <td><?php echo $data['bdate']; ?></td>
                <td><?php echo $data['lastComes']; ?></td>
                <td><?php echo $data['howComes']; ?></td>
                <td><a href='http://vk.com/id<?php echo $data['id']; ?>' target='_blank'>http://vk.com/id<?php echo $data['id']; ?></a></td>
                <td><a href='http://vk.com/im?sel=<?php echo $data['id']; ?>' target='_blank' class='btn btn-mini btn-primary'>Написать</a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>


      </div>
    </div>
  </body>
</html>
