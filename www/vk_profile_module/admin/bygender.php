<?php
  include '_helpers.php';
  $datas = get_data();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <title>Админка профилей ВК посетителей сайта "Антей"</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.9.7/chartist.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.9.7/chartist.min.js"></script>
    <link rel='stylesheet' href='style.css'>
  </head>
  <body>
    <div class='container-fluid'>
      <div class='row'>

        <div class='col-xs-3 sidebar'>
          <div class='nav'>
            <li><a href='index.php'>Список посетителей</a></li>
            <li><a href='thebest.php'>Наиболее часто посещающие</a></li>
          </div>
          <br>
          <h4 class='p-l-15'>Статистика</h4>
          <div class='nav'>
            <li><a href='byhow.php'>По количеству посещений</a></li>
            <li class='active'><a href='bygender.php'>По полу/возрасту</a></li>
          </div>
        </div>

        <div class='col-xs-9 col-xs-offset-3 content'>
          <h4>Пол/возраст за все время</h4>
          <div id='gender_all_bar'></div>
          <h4>Пол/возраст за месяц</h4>
          <div id='gender_month_bar'></div>
          <br>
          <ul>
            <li>1-й столбец - не указали пол</li>
            <li>2-й столбец - мужской</li>
            <li>3-й столбец - женский</li>
          </ul>

          <script>
            <?php echo 'var data = '.json_encode($datas).';'; ?>
            var ages = [
              {from: 0, to: 17},
              {from: 18, to: 23},
              {from: 24, to: 30},
              {from: 31, to: 45},
              {from: 45, to: 80}
            ];
            var series = [[0,0,0,0,0,0],
                          [0,0,0,0,0,0],
                          [0,0,0,0,0,0]];
            var seriesMonth = [[0,0,0,0,0,0],
                          [0,0,0,0,0,0],
                          [0,0,0,0,0,0]];
            data.forEach(function(item){
              var sex = 0;
              if (item.sex != '-') sex = item.sex === 'муж' ? 2 : 1;
              var ageCat = 5;
              if (item.age != '-')
                ages.forEach(function(a, i){
                  if (item.bdate >= a.from && item.bdate <= a.to)
                    ageCat = i;
                });
              ++ series[sex][ageCat];
              if ((Date.now() - item.rawTime * 1000) / 1000 / 3600 / 24 < 30)
                ++ seriesMonth[sex][ageCat];
            });
            new Chartist.Bar('#gender_all_bar', {
              labels: ['до 17', '18-23', '24-30', '31-45', '45 и больше', 'не указали возраст'],
              series: series
            });
            new Chartist.Bar('#gender_month_bar', {
              labels: ['до 17', '18-23', '24-30', '31-45', '45 и больше', 'не указали возраст'],
              series: seriesMonth
            });
          </script>
        </div>

      </div>
    </div>
  </body>
</html>
